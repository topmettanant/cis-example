from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [

    url('', include('social.apps.django_app.urls', namespace='social')),
    url('', include('django.contrib.auth.urls', namespace='auth')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'home.views.index', name='index'),
    url(r'^profile/', include('user_profile.urls')),
    url(r'^service/', include('service.urls')),

]
