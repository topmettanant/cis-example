from django import forms
from service.models import ServiceRequest, ServiceTeam


class ServiceRequestForm(forms.ModelForm):
    message = forms.CharField(
        max_length=256,
        label="ข้อความ",
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': '3'})
    )
    team = forms.ModelChoiceField(
        ServiceTeam.objects.all(),
        label='ฝ่าย',
        widget=forms.Select(attrs={'class': 'form-control'})
    )

    priority_list = [(1, 'น้อย'), (2, 'ปานกลาง'), (3, 'มาก'), (4, 'ด่วนที่สุด')]
    priority = forms.ChoiceField(choices=priority_list, label='ความเร่งด่วน', widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = ServiceRequest
        fields = ('message', 'team', 'priority')
