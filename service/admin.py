from django.contrib import admin
from service.models import ServiceRequest, ServiceTeam

# Register your models here.
admin.site.register(ServiceRequest)
admin.site.register(ServiceTeam)
