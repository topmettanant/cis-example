from django import template
from django.db.models import Q
import datetime
from service.models import ServiceRequest, ServiceTeam

register = template.Library()


@register.inclusion_tag('service/menu_list.html')
def get_service_menu(user_details, current_menu, badge=0):
    context = {'user_details': user_details, 'current_menu': current_menu, 'badge': badge}
    return context


@register.inclusion_tag('service/day_name.html')
def get_day_name(date):
    today = datetime.date.today()
    delta = datetime.timedelta(days=1)
    if date == today:
        day = 'วันนี้'
    elif date == today - delta:
        day = 'เมื่อวาน'
    else:
        day = date.strftime('%d-%m-%y')
    return {'day': day}


@register.inclusion_tag('service/request_panel.html')
def get_request_panel(request):
    return {'service_request': request}


@register.inclusion_tag('service/support_request_panel.html')
def get_support_request_panel(request):
    return {'service_request': request}


@register.inclusion_tag('service/badge.html')
def get_service_support_badge(user_details, active=None):
    my_teams = []
    for team in ServiceTeam.objects.all():
        if user_details in team.officers.all():
            my_teams.append(team)
    my_requests = []
    for service_request in ServiceRequest.objects.filter(status=0):
        if service_request.team in my_teams:
            my_requests.append(service_request)
    my_requests += ServiceRequest.objects.filter(Q(status=1) & Q(support=user_details))
    return {'badge': len(my_requests), 'active': active}


@register.inclusion_tag('service/badge.html')
def get_service_score_badge(user_details, active=None):
    my_requests = ServiceRequest.objects.filter(status=2).filter(user_details=user_details)
    return {'badge': len(my_requests), 'active': active}
