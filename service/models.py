from django.db import models
from user_profile.models import UserDetails


class ServiceTeam(models.Model):
    officers = models.ManyToManyField(UserDetails)
    job = models.CharField(max_length=256)

    def __str__(self):
        return self.job

    def save(self, *args, **kwargs):
        super(ServiceTeam, self).save(*args, **kwargs)
        for officer in self.officers.all():
            officer.is_service_support = True
            officer.save()


class ServiceRequest(models.Model):
    user_details = models.ForeignKey(UserDetails, related_name='service_user')
    message = models.CharField(max_length=256)
    time = models.DateTimeField()
    team = models.ForeignKey(ServiceTeam)
    priority = models.IntegerField(default=2)  # priority 4:ด่วนที่สุด, 3:มาก, 2:ปานกลาง, 1:น้อย
    status = models.IntegerField(default=0)  # status: 0:รอดำเนินการ, 1:ดำเนินการ, 2:เสร็จแล้ว, 3:ประเมินแล้ว

    support = models.ForeignKey(UserDetails, related_name='service_support', null=True)
    support_time = models.DateTimeField(null=True)
    finish_time = models.DateTimeField(null=True)
    rate = models.IntegerField(null=True)  # rate 1-5

    def __str__(self):
        return self.message
