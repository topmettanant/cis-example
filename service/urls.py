from django.conf.urls import url
from service import views

urlpatterns = [

    url(r'^$', views.index, name='service_index'),
    url(r'^create_request/$', views.create_request, name='create_service_request'),
    url(r'^request/(?P<request_id>[0-9]+)/delete/$', views.delete_request, name='delete_service_request'),
    url(r'^request/(?P<request_id>[0-9]+)/receive/$', views.receive_request, name='receive_service_request'),
    url(r'^request/(?P<request_id>[0-9]+)/done/$', views.complete_request, name='complete_service_request'),
    url(r'^request/(?P<request_id>[0-9]+)/score/(?P<score>[1-5])/$', views.score_request, name='score_service_request'),
    url(r'^history/$', views.history, name='service_history'),
    url(r'^support/$', views.support, name='service_support'),
    url(r'^support/history/$', views.support_history, name='service_support_history'),

]
