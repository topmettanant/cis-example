from django.contrib.auth.models import User
from user_profile.models import UserDetails


def accessible_from(user):
    user_details = None
    if type(user) is User:
        user_details = UserDetails.objects.get(user=user)
    elif type(user) is UserDetails:
        user_details = user

    if user_details.is_service_user or user_details.is_service_support or user_details.is_service_manager:
        return True
    else:
        return False


def get_app_info():
    url_name = 'service_index'
    app_text = 'บริการ'
    app_icon = 'wrench'
    return {'url': url_name, 'name': app_text, 'icon': app_icon}
