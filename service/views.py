from django.shortcuts import render, redirect, render_to_response
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required
from service.info import *
from service.forms import ServiceRequestForm
from django.utils import timezone
from django.core.mail import send_mail
from service.models import ServiceRequest, ServiceTeam
from service.mail_template import html_mail
from django.db.models import Q


def get_user_info(request):
    user = request.user
    user_details = UserDetails.objects.get(user=user)
    return user_details, accessible_from(user_details)


@login_required(login_url='/')
def index(request):
    user_details, accessible = get_user_info(request)
    if user_details.is_service_user:
        info = get_app_info()
        my_requests = list(ServiceRequest.objects.filter(status=2).filter(user_details=user_details).order_by('-time'))
        my_requests += list(ServiceRequest.objects.filter(user_details=user_details)
                            .filter(Q(status=0) | Q(status=1)).order_by('-time'))
        context = {'user_details': user_details, 'app': info, 'current_menu': 'index', 'requests': my_requests}
        return render(request, 'service/index.html', context)
    else:
        return redirect('/')


@login_required(login_url='/')
def create_request(request):
    user_details, accessible = get_user_info(request)
    if user_details.is_service_user:
        if request.method == 'POST':
            form = ServiceRequestForm(request.POST)
            if form.is_valid():
                service_request = form.save(commit=False)
                service_request.user_details = user_details
                service_request.time = timezone.now()
                service_request.save()
                team = service_request.team
                officers = team.officers.all()
                officer_mails = []
                for officer in officers:
                    officer_mails.append(officer.user.email)
                html_message = html_mail.replace('{title}', 'คำขอรับบริการใหม่')
                html_message = html_message.replace('{header}', 'คำขอรับบริการจาก ' + str(service_request.user_details))
                priority_text = ''
                if service_request.priority == 1:
                    priority_text = 'น้อย'
                elif service_request.priority == 2:
                    priority_text = 'ปานกลาง'
                elif service_request.priority == 3:
                    priority_text = 'มาก'
                elif service_request.priority == 4:
                    priority_text = 'ด่วนที่สุด!'
                html_message = html_message.replace('{message}', 'ความเร่งด่วน: ' + priority_text
                                                    + '<br/><br/>' + service_request.message)
                html_message = html_message.replace('{contact}',
                                                    'ติดต่อผู้รับบริการ<br/>อีเมล: ' +
                                                    user_details.user.email +
                                                    '<br/>หมายเลขภายใน: ' +
                                                    user_details.internal_number +
                                                    '<br/>โทรศัพท์มือถือ: ' +
                                                    user_details.mobile_phone_number)
                html_message = html_message.replace('{button_link}', 'http://cis.eng.src.ku.ac.th:8080/service/')
                html_message = html_message.replace('{button_text}', 'รายละเอียด')
                send_mail('คำร้องขอใช้บริการใหม่', service_request.message, 'cis@eng.src.ku.ac.th', officer_mails,
                          fail_silently=False, html_message=html_message)
                return redirect('/service/')
            else:
                context = {'form': form, 'user_details': user_details, 'app': get_app_info(),
                           'current_menu': 'create_request'}
                return render_to_response('service/create_request.html',
                                          context, context_instance=RequestContext(request))

        else:
            form = ServiceRequestForm(initial={'priority': '2'})
            context = {'form': form, 'user_details': user_details, 'app': get_app_info(),
                       'current_menu': 'create_request'}
            return render(request, 'service/create_request.html', context)
    else:
        return redirect('/')


@login_required(login_url='/')
def delete_request(request, request_id):
    user_details, accessible = get_user_info(request)
    service_request = ServiceRequest.objects.filter(id=request_id)[0]
    if service_request.user_details == user_details and service_request.status == 0:
        service_request.status = -1
        service_request.save()
        context = {'service_request': service_request}
        return render(request, 'service/delete_request_panel.html', context)
    else:
        return redirect('/')


@login_required(login_url='/')
def receive_request(request, request_id):
    user_details, accessible = get_user_info(request)
    service_request = ServiceRequest.objects.filter(id=request_id)[0]
    team = service_request.team
    if user_details in team.officers.all():
        if service_request.status == 0:
            service_request.support = user_details
            service_request.support_time = timezone.now()
            service_request.status = 1
            service_request.save()

            # -- add if want to send mail to user that the request is received --

            return render(request, 'service/support_request_panel.html', {'service_request': service_request})

        else:
            return render(request, 'service/error_panel.html', {'service_request': service_request,
                                                                'error_message': 'ขออภัย มีเจ้าหน้าที่ท่านอื่นรับคำร้องนี้แล้ว'})
    else:
        return redirect('/')


@login_required(login_url='/')
def complete_request(request, request_id):
    user_details = get_user_info(request)[0]
    service_request = ServiceRequest.objects.filter(id=request_id)[0]
    if service_request.support == user_details:
        service_request.finish_time = timezone.now()
        service_request.status = 2
        service_request.save()

        # -- add if want to send mail to user that the request is done --
        html_message = html_mail.replace('{title}', 'คำร้องขอรับบริการดำเนินการเสร็จสิ้นแล้ว')
        html_message = html_message.replace('{header}', 'คำร้องของคุณดำเนินการเสร็จสิ้นแล้ว')
        html_message = html_message.replace('{message}', 'คำร้อง<br/><br/>' +
                                            service_request.message + '<br/><br/>' +
                                            'ผู้ดำเนินการ: ' + str(service_request.support) +
                                            ' ได้ดำเนินการเสร็จเรียบร้อยแล้ว')
        html_message = html_message.replace('{contact}', 'กรุณาประเมินความพึงพอใจของการให้บริการในครั้งนี้')
        html_message = html_message.replace('{button_link}', 'http://cis.eng.src.ku.ac.th:8080/service/')
        html_message = html_message.replace('{button_text}', 'รายละเอียด')
        send_mail('คำร้องของคุณดำเนินการเสร็จสิ้นแล้ว', service_request.message, 'cis@eng.src.ku.ac.th',
                  [service_request.user_details.user.email], fail_silently=False, html_message=html_message)

        return render(request, 'service/complete_request_panel.html', {'service_request': service_request})
    else:
        return redirect('/')


@login_required(login_url='/')
def score_request(request, request_id, score):
    user_details = get_user_info(request)[0]
    service_request = ServiceRequest.objects.filter(id=request_id)[0]
    if service_request.user_details == user_details and service_request.status == 2:
        service_request.rate = score
        service_request.status = 3
        service_request.save()
        return render(request, 'service/score_request_panel.html', {})
    else:
        return redirect('/')


@login_required(login_url='/')
def history(request):
    user_details, accessible = get_user_info(request)
    if user_details.is_service_user:
        info = get_app_info()
        my_requests = list(ServiceRequest.objects.filter(user_details=user_details)
                           .filter(Q(status=-1) | Q(status=3)).order_by('-time'))
        context = {'user_details': user_details, 'app': info, 'current_menu': 'history', 'requests': my_requests}
        return render(request, 'service/history.html', context)
    else:
        return redirect('/')


@login_required(login_url='/')
def support(request):
    user_details, accessible = get_user_info(request)
    if user_details.is_service_support:
        info = get_app_info()
        my_teams = []
        for team in ServiceTeam.objects.all():
            if user_details in team.officers.all():
                my_teams.append(team)
        my_requests = []
        for service_request in ServiceRequest.objects.filter(status=0):
            if service_request.team in my_teams:
                my_requests.append(service_request)
        my_requests += ServiceRequest.objects.filter(Q(status=1) & Q(support=user_details))
        my_requests.sort(key=lambda r: r.time, reverse=True)
        context = {'user_details': user_details, 'app': info, 'current_menu': 'support', 'requests': my_requests}
        return render(request, 'service/support.html', context)
    else:
        return redirect('/')


@login_required(login_url='/')
def support_history(request):
    user_details = get_user_info(request)[0]
    if user_details.is_service_support:
        info = get_app_info()
        my_services = list(ServiceRequest.objects.filter(support=user_details)
                           .filter(Q(status=2) | Q(status=3)).order_by('-time'))
        context = {'user_details': user_details, 'app': info, 'current_menu': 'support_history',
                   'service_requests': my_services}
        return render(request, 'service/support_history.html', context)
    else:
        return redirect('/')
