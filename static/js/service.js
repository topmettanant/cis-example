$(document).ready(function(){
    $('.fa-spinner').toggleClass('hidden');
});

$('.btn-primary').click(function() {
    $('.fa-spinner').toggleClass('hidden');
});

$('.delete-request').click(function(){
    var request_id = $(this).attr('request-id');

    $.get('/service/request/'+request_id+'/delete/', {}, function(data){
        $('#panel-request-'+request_id).html(data);
    });
});

$('.score-service-request').click(function(){
    var request_id = $(this).attr('request-id');
    var score = $('#service-star-score-'+ request_id).val();
    if(score) {
        $(this).html(' <i class="fa fa-spinner fa-spin"></i>');

        $.get("/service/request/" + request_id + "/score/" + score, {}, function (data) {
            $('#panel-request-' + request_id).html(data);
        });
    }
});